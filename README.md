- 👋 Hello everyone, Welcome to my profile.

<!---
John is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes. 
--->

✔ 100% Job Success (I have worked on 4 remotely projects untill now)

✔ 7+ years of experience

✸ 𝐂𝐋𝐈𝐄𝐍𝐓𝐒 𝐅𝐄𝐄𝐃𝐁𝐀𝐂𝐊 ✸

⭐"They are really professional, prompt, and out-of-the-box thinkers."
⭐"They valued their clients and answered all queries very promptly."
⭐"Exceptional work by an exceptional team. I highly recommend them. We will continue using them on all future projects."

✸ 𝐒𝐄𝐑𝐕𝐈𝐂𝐄𝐒 ✸

Web development

      ✔ Frontend - ReactJs | Angular | JavaScript | Typescript | HTML5/CSS/SCSS | Bootstrap | Tailwind | Material UI
      ✔ Backend - NodeJS | Larvel, CI, Symfony | MYSQL | NoSQL DB | SQLite
      ✔ CMS - Shopify, Webflow
      
Cloud Computing

      ✔ Microsoft Azure - DevOps, Docker, Azure Functions, CI/CD
      ✔ Amazon-Cloud solutions AWS (EC2, S3, ELB, Lambda, DynamoDB)
      ✔ GitHub, Bitbucket, JIRA, Jenkins

